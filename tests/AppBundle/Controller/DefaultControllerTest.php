<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
            $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }

    public function testRandom()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/random');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertGreaterThan(0, $crawler->filter('#number')->text());
        if ($crawler->filter('#number')->text() > 98) {
            	$this->assertLessThan(100, $crawler->filter('#number')->text());
        }
    }
}
